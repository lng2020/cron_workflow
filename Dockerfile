# Use an official Ubuntu runtime as the base image
FROM ubuntu:latest

# Run the command that echoes "Hello Gitea"
CMD echo "Hello Gitea"